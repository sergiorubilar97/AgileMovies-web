import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IUserSession } from 'src/app/global/models/user-models/user-session.model';
import { AuthService } from 'src/app/global/services/auth-services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Output() cancelEvent = new EventEmitter<boolean>();

  isErrorLogin: boolean;
  isLoadingLogin: boolean;
  isSetToken: boolean;
  loginForm: FormGroup;
  returnUrl: string;
  errorMessage: string;
  subscribeLogin: Subscription;

  constructor(private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _router: Router) { }

  ngOnInit(): void {
    this.initVariables();
  }

  ngOnDestroy(): void {
    if (this.subscribeLogin) {
      this.subscribeLogin.unsubscribe();
    }
  }

  initVariables() {
    this.isErrorLogin = false;
    this.isLoadingLogin = false;
    this.errorMessage = '';
    this.loginForm = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get f() { return this.loginForm.controls; }

  userLogin() {
    this.isErrorLogin = false
    if (this.loginForm.invalid) {
      return;
    }
    this.isLoadingLogin = true;
    this.subscribeLogin = this._authService.login(this.loginForm.value.username, this.loginForm.value.password).subscribe((user: IUserSession) => {
      user.isLogged = true;
      console.log(this.subscribeLogin);
      this._authService.updateAndEmitCurrentUser(user);
      this.cancelEvent.emit(true);
      this._router.navigateByUrl('/home');
    },
      error => {
        this.errorMessage = 'Usuario o contraseña incorrectos';
        this.isErrorLogin = true;
        this.isLoadingLogin = false;
      });
  }

  cancel() {
    this.cancelEvent.emit(true);
  }
}
