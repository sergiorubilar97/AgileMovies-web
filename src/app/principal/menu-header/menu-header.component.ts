import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/global/services/auth-services/auth.service';
import { UserService } from 'src/app/global/services/user-services/user.service';
import { IUserSession } from '../../global/models/user-models/user-session.model';

@Component({
  selector: 'app-menu-header',
  templateUrl: './menu-header.component.html',
  styleUrls: ['./menu-header.component.scss']
})
export class MenuHeaderComponent implements OnInit {

  currentUser$: Observable<IUserSession>;
  userSession: IUserSession;

  constructor(private auth: AuthService,
              private user: UserService,
              private router: Router) { }

  ngOnInit(): void {
    this.currentUser$ = this.auth.getCurrentUserObs();
    // this.initVariables();
    // this.initInfoUser();
  }

  initVariables(): void {

  }

  async initInfoUser() {
    debugger
    const infoUser = await this.user.getUser().toPromise();
    if (!infoUser || typeof infoUser === undefined || infoUser === null) {
      this.currentUser$ = this.auth.getCurrentUserObs();
    } else {
      this.userSession = {
        id: infoUser.data.id,
        email: infoUser.data.email,
        firstName: infoUser.data.firstName,
        lastName: infoUser.data.lastName,
        createdAt: infoUser.data.createdAt
      }
      this.auth.updateCurrentUser(this.userSession);
      this.currentUser$ = this.auth.getCurrentUserObs();
    }
  }
  logout(): boolean {
    this.auth.logout();
    this.router.navigate(['login']);
    return false;
  }
}
