import { HttpClient } from '@angular/common/http';
import { timeout } from 'rxjs/operators';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { ApiParams } from '../models/http-modeels/http.model';

@Injectable({
    providedIn: 'root'
})
export class HttpProvider {
    @Output() httpEmitter = new EventEmitter();
    timeout = 30000;
    currentApiPromises: Promise<any>[] = [];
    timeOutPromise = 10000;
    constructor(public httpClient: HttpClient,
    ) { }

    get(params: ApiParams, responsePath?: string, func?: string, page?: string) {

        return new Promise<any>((resolve, reject) => {
            this.httpClient.get(params.url, params.httpOptions).pipe(
            ).subscribe((res: any) => {
                resolve(res);
            }, err => {
                this.rejectOrResolveError(err, params.rejectError, resolve, reject, func, page);
            });
        });

    }

    rejectOrResolveErrorGet(error: any, rejectError: any, resolve: any, reject: any, func?: string, page?: string, id?: string) {

        if (page && func) {
        }
        if (rejectError) {
            resolve(error.error);
        } else {
            resolve(false);
        }
        setTimeout(() => {
            delete this.currentApiPromises[id];
        }, this.timeOutPromise);
    }

    post(params: ApiParams, responsePath?: string, func?: string, page?: string): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            this.httpClient.post(params.url, params.body, params.httpOptions).pipe(
                timeout(params.tout)
            ).subscribe((res: any) => {
                resolve(res);
            }, err => {
                this.rejectOrResolveError(err, params.rejectError, resolve, reject, func, page);
            });
        });

    }

    rejectOrResolveError(error: any, rejectError: any, resolve: any, reject: any, func?: string, page?: string, curl?: any) {
        if (page && func) {
        }
        if (rejectError) {
            resolve(error.error);
        } else {
            resolve(false);
        }
    }
}
