import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { ApiParams } from '../../models/http-modeels/http.model';
import { HttpProvider } from '../../providers/http.provider';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor( private httpProvider: HttpProvider ) { }


  getPopularMoviesCall(page: string) {
    const params: ApiParams = {
      url: environment.POPULAR_MOVIES,
      httpOptions: {
        params: { page: page},
        headers: new HttpHeaders ({
          'Content-Type': 'application/json',
          Accept: '*/*'
        })
      }
    };
    return this.httpProvider.get(params);
  }

  getNowMoviesCall(page: string) {
    const params: ApiParams = {
      url: environment.NOW_PLAYING,
      httpOptions: {
        params: { page: page },
        headers: new HttpHeaders ({
          'Content-Type': 'application/json',
          Accept: '*/*'
        })
      }
    }
    return this.httpProvider.get(params);
  }

  getActorsByIdCall(id: string) {
    const params: ApiParams = {
      url: environment.ACTOR_MOVIES.replace('{movieId}', id),
      httpOptions: {
        headers: new HttpHeaders ({
          'Content-Type': 'application/json',
          Accept: '*/*'
        })
      }
    }
    return this.httpProvider.get(params);
  }

  getPopularMovies(page: string) {
    return new Observable ((observer: Observer<any>) => {
      const resp = this.getPopularMoviesCall(page);
      observer.next(resp);
      observer.complete();
    });
  }

  getNowMovies(page: string) {
    return new Observable ((observer: Observer<any>) => {
      const resp = this.getNowMoviesCall(page);
      observer.next(resp);
      observer.complete();
    });
  }

  getActorsById(id: string) {
    return new Observable ((observer: Observer<any>) => {
      const resp = this.getActorsByIdCall(id);
      observer.next(resp);
      observer.complete();
    });
  }
}
