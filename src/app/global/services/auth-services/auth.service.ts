import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { IUserSession } from '../../models/user-models/user-session.model';
import { IAuthenticationResponse } from '../../models/auth-models/auth-response.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject = new BehaviorSubject<IUserSession>({} as IUserSession);
  private currentUser$: Observable<IUserSession>;

  constructor(private httpClient: HttpClient) {
    this.currentUser$ = this.currentUserSubject.asObservable();
  }


  public get currentUserValue(): IUserSession {
    return this.currentUserSubject.getValue();
  }

  getCurrentUserSubject(): BehaviorSubject<IUserSession> {
    return this.currentUserSubject;
  }

  getCurrentUserObs(): Observable<IUserSession> {
    return this.currentUser$;
  }

  login(username: string, password: string): Observable<IUserSession> {
    return this.httpClient
      .post<IAuthenticationResponse>(
        `${environment.AUTH}`,
        { username, password },
        httpOptions
      )
      .pipe(
        map((payload: IAuthenticationResponse) => {
          return {
            email: payload.data.user.email,
            firstName: payload.data.user.firstName,
            lastName: payload.data.user.lastName,
            token: payload.data.payload.token,
            refresh_token: payload.data.payload.refresh_token,
            isLogged: true,
          } as IUserSession;
        }),
        catchError(this.handleError)
      );
  }

  getAuthToken(): string {
    return this.currentUserSubject.getValue().token;
  }

  logout(): void {
    const user = {} as IUserSession;
    this.updateAndEmitCurrentUser(user);
  }

  isLogged(): Observable<boolean> {
    return this.currentUser$.pipe(
      map((user: IUserSession) => user.isLogged),
    );
  }

  updateCurrentUser(user: IUserSession): void {
    this.updateAndEmitCurrentUser(user);
  }

  updateAndEmitCurrentUser(user: IUserSession): void {
    localStorage.setItem('currentUser', JSON.stringify(user));
    this.currentUserSubject.next(user);
  }

  handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      return throwError(error.error.message);
    }
    return throwError(error);
  }

  refreshToken(): Observable<IUserSession> {

    const refreshToken = this.currentUserValue.refresh_token;
    return this.httpClient
      .post<IAuthenticationResponse>(
        `${environment.REFRESH_TOKEN}`,
        { refresh_token: refreshToken },
        httpOptions
      )
      .pipe(
        map((payload: IAuthenticationResponse) => {
          console.log('payload', payload);

          return {
            token: payload.data.payload.token
          };
        },
        catchError(this.handleError)
      ));
  }
}
