export interface IAuthenticationResponse {
    status: string,
    data: {
      user: {
        email: string
        firstName: string
        lastName: string
      }
      payload: {
        type: string
        token: string
        refresh_token?: string
      }
    }
}