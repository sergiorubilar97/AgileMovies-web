export interface ApiParams {
    url: string;
    httpOptions?: any;
    body?: any;
    tout?: number;
    rejectError?: boolean;
}
