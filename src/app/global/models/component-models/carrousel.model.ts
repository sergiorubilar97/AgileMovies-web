export const Carrousel = {
    initialSlide: 0,
    autoplay: true,
    speed: 250,
    centeredSlides: false,
    loop: false,
    slidesPerView: 2,
    spaceBetween: 10,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
    breakpoints: {
        567: {
            slidePerView: 5.2
        },
        767: {
            slidesPerView: 3.5,
            spaceBetween: 10
          }
    }
}
