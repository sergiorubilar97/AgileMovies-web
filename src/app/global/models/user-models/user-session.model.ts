export interface IUserSession {
    id?: string;
    username?: string;
    password?: string;
    email?: string;
    firstName?: string;
    lastName?: string;
    token?: string;
    refresh_token?: string;
    isLogged?: boolean;
    createdAt?: string;
}
