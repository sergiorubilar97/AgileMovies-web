export interface Movie {
    id: string;
    title: string;
    full_backdrop_path: string;
    full_poster_path: string;
    overview: string;
}
