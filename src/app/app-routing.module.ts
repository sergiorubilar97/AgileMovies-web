import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { DashboardComponent } from './sections/dashboard/dashboard.component';
import { LoggedGuard } from './global/guards/logged.guard';
import { LoginComponent } from './principal/login/login.component';
import { MovieDetailComponent } from './sections/movie-detail/movie-detail.component';

const routes: Routes = [
  {
    path: 'home',
    component: DashboardComponent,
    canActivate: [LoggedGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'movie-detail',
    component: MovieDetailComponent
  },
  {
    path: '**',
    redirectTo: '/'
  }
];
  @NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
