import { Component, OnInit } from '@angular/core';
import { Actor } from 'src/app/global/models/movies-models/actor.model';
import { Movie } from 'src/app/global/models/movies-models/movie.model';
import { DataSharedService } from 'src/app/global/services/movies-services/data-shared.service';
import { MoviesService } from 'src/app/global/services/movies-services/movies.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {
  movieId: string;
  movieData: Movie;
  actorData: any;
  actorArray: any;
  isErrorActors: boolean;

  constructor(private _movieService: MoviesService,
              private _dataService: DataSharedService) { }

  ngOnInit(): void {
    this.initVariables();
    this.getActorsById();
  }

  initVariables() {
    this._dataService.sharedParam.subscribe( param => this.movieData = param);
    this.isErrorActors = false;
    this.actorData = [];
    this.actorArray = [];
  }

  async getActorsById() {
    const actors = await this._movieService.getActorsById(this.movieData.id).toPromise();
    if (!actors || typeof actors === 'undefined' || actors === null) {
      this.isErrorActors = true;
    } else {
      this.actorData = this.loadInfoActors(actors);
    }
  }

  loadInfoActors(actors: any): Actor {
    let actorItem: Actor;
    for (const i in actors.data) {
      if (actors.data.hasOwnProperty(i)) {
      actorItem = {
        id: actors.data[i].id,
        name: actors.data[i].name,
        profile_path: actors.imageBaseUrl + actors.data[i].profile_path
      }
      this.actorArray.push(actorItem);
      }
    }
    return this.actorArray;
  }

  
}
