import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Carrousel } from 'src/app/global/models/component-models/carrousel.model';
import { Movie } from 'src/app/global/models/movies-models/movie.model';
import { AuthService } from 'src/app/global/services/auth-services/auth.service';
import { DataSharedService } from 'src/app/global/services/movies-services/data-shared.service';
import { MoviesService } from 'src/app/global/services/movies-services/movies.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  slideOpts: any;
  popularPage: number;
  nowPage: number;
  popularMovies: any;
  nowMovies: any;
  isLoadingPopularMovies: boolean;
  isLoadingNowMovies: boolean;
  isErrorPopularMovies: boolean;
  isErrorNowMovies: boolean;
  popularMoviesArray = [];
  nowMoviesArray = [] ;
  isLogged$: Observable<boolean>;

  constructor(private _moviesServie: MoviesService,
              private _router: Router,
              private _dataService: DataSharedService,
              private _authService: AuthService) {
              }

  ngOnInit(): void {
    this.initVariables();
    this.initNowMovies(this.nowPage);
    this.initPopularMovies(this.popularPage);
  }

  initVariables() {
    this.slideOpts = Carrousel;
    this.popularPage = 1;
    this.nowPage = 1;
    this.isLoadingPopularMovies = true;
    this.isErrorPopularMovies = false;
  }

  async initPopularMovies(page: number) {
    const popularMovies = await this._moviesServie.getPopularMovies(String(this.popularPage)).toPromise();
    if (!popularMovies || typeof popularMovies === 'undefined' || popularMovies === null) {
      this.isErrorPopularMovies = true;
    } else {
      this.popularMovies = this.loadPopularMovies(popularMovies);
    }
    this.isLoadingPopularMovies = false;
  }

  async initNowMovies(page: number) {
    const nowMovies = await this._moviesServie.getNowMovies(String(this.nowPage)).toPromise();
    if (!nowMovies || typeof nowMovies === 'undefined' || nowMovies === null) {
      this.isErrorNowMovies = true;
    } else {
      this.nowMovies = this.loadNowMovies(nowMovies);
    }
    this.isLoadingNowMovies = false;
  }

  loadPopularMovies(popularMovies: any) {
    let popularMoviesItem: Movie;
    let full_backdrop_path: string;
    let full_poster_path: string;
    for (const i in popularMovies.data) {
      if (popularMovies.data.hasOwnProperty(i)) {
        if (popularMovies.data[i].backdrop_path === null) {
          full_backdrop_path = 'http://imgclasificados4.emol.com/7229435_0/042/F152223413131104109139199197721172374218042.png';
        } else if (popularMovies.data[i].poster_path === null) {
          full_poster_path = 'http://imgclasificados4.emol.com/7229435_0/042/F152223413131104109139199197721172374218042.png';
        } else {
          full_backdrop_path = popularMovies.imageBaseUrl + popularMovies.data[i].backdrop_path;
          full_poster_path = popularMovies.imageBaseUrl + +popularMovies.data[i].poster_path;
        }
        popularMoviesItem = {
          id: popularMovies.data[i].id,
          title: popularMovies.data[i].title,
          full_backdrop_path: full_backdrop_path,
          full_poster_path: full_poster_path,
          overview: popularMovies.data[i].overview
        }
        this.popularMoviesArray.push(popularMoviesItem);
      }
    }
    return this.popularMoviesArray
  }

  loadNowMovies(nowMovies: any) {
    let nowMoviesItem: Movie;
    let full_backdrop_path: string;
    let full_poster_path: string;
    for (const i in nowMovies.data) {
      if (nowMovies.data.hasOwnProperty(i)) {
        if (nowMovies.data[i].backdrop_path === null || nowMovies.data[i].backdrop_path === '') {
          full_backdrop_path = 'http://imgclasificados4.emol.com/7229435_0/042/F152223413131104109139199197721172374218042.png';
        } else if (nowMovies.data[i].poster_path === null || nowMovies.data[i].poster_path === '') {
          full_poster_path = 'http://imgclasificados4.emol.com/7229435_0/042/F152223413131104109139199197721172374218042.png';
        } else {
          full_backdrop_path = nowMovies.imageBaseUrl + nowMovies.data[i].backdrop_path;
          full_poster_path = nowMovies.imageBaseUrl + nowMovies.data[i].poster_path;
        }
        nowMoviesItem = {
          id: nowMovies.data[i].id,
          title: nowMovies.data[i].title,
          full_backdrop_path: full_backdrop_path,
          full_poster_path: full_poster_path,
          overview: nowMovies.data[i].overview
        }
        this.nowMoviesArray.push(nowMoviesItem);
      }
    }
    return this.nowMoviesArray;
  }

  async loadPopularData(event: any) {
    if (event) {
      this.initPopularMovies(this.popularPage++);
      event.target.complete();
    }
  }

  async loadNowData(event: any) {
    if (event) {
      this.initNowMovies(this.nowPage++);
      event.target.complete();
    }
  }

  async goToDetail(movie: any) {
    this._dataService.changeParams(movie);
    this._router.navigate(['movie-detail']);
  }


}
