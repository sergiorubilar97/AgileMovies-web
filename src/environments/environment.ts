export const environment = {
  production: false,

  /** Login APIS */
  AUTH: 'http://161.35.140.236:9005/api/auth/login',
  REFRESH_TOKEN: 'http://161.35.140.236:9005/api/auth/refresh',

  /** User APIS */
  USER_INFO: 'http://161.35.140.236:9005/api/user/me',

  /** Movie APIS */
  POPULAR_MOVIES: 'http://161.35.140.236:9005/api/movies/popular',
  NOW_PLAYING: 'http://161.35.140.236:9005/api/movies/now_playing',
  ACTOR_MOVIES: 'http://161.35.140.236:9005/api/movies/{movieId}/actors'
};